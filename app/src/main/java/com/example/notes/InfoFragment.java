package com.example.notes;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.notes.database.entity.Note;
import com.example.notes.dummy.DummyContent;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InfoFragment extends Fragment {

    public interface OnInfoFragmentListener {
        void onBtnBackClick();
        void onBtnDeleteClick();
    }

    private OnInfoFragmentListener listener;
    TextView txtDateInfo;
    TextView txtDescInfo;
    Note item;
    Button btnBack, btnDelete;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public InfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InfoFragment newInstance(String param1, String param2) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        txtDateInfo = view.findViewById(R.id.txtDateInfo);
        txtDescInfo = view.findViewById(R.id.txtDescInfo);
        btnBack = view.findViewById(R.id.btnBackInfo);
        btnDelete = view.findViewById(R.id.btnDeleteInfo);

        txtDateInfo.setText(item.getDateTime(this.getContext()));
        txtDescInfo.setText(item.getDesc());

        btnBack.setOnClickListener(v -> {
            listener.onBtnBackClick();
        });
        btnDelete.setOnClickListener(v -> {
            DummyContent.ITEMS.remove(item);
            item.delete();
            listener.onBtnDeleteClick();
        });
        return view;
    }

    public void setData(Note item) {
        this.item = item;
        Log.d("TAG", "IOException : ");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof OnInfoFragmentListener) {
            listener = (OnInfoFragmentListener) context;
        }
    }
}