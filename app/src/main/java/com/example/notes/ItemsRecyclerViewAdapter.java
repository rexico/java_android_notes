package com.example.notes;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.notes.database.entity.Note;

import java.util.List;


public class ItemsRecyclerViewAdapter
        extends RecyclerView.Adapter<ItemsRecyclerViewAdapter.ViewHolder> {

    public interface OnItemsRecyclerViewAdapterListener {
        void onItemClick(Note item);
    }

    private final List<Note> mValues;
    private OnItemsRecyclerViewAdapterListener listener;

    public ItemsRecyclerViewAdapter(OnItemsRecyclerViewAdapterListener listener, List<Note> items) {
        mValues = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.txtNum.setText(mValues.get(position).getId().toString());
        holder.txtDate.setText(mValues.get(position).getDateTime(holder.txtDate.getContext()));
        holder.txtDesc.setText(mValues.get(position).getDesc());

        holder.mView.setOnClickListener(v -> {
            listener.onItemClick(mValues.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView txtNum;
        public final TextView txtDate;
        public final TextView txtDesc;

        public Note mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtNum = view.findViewById(R.id.txtNum);
            txtDate = view.findViewById(R.id.txtDate);
            txtDesc = view.findViewById(R.id.txtDesc);
        }

    }
}