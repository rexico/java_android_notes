package com.example.notes.dummy;

import com.example.notes.database.entity.Note;

import org.w3c.dom.Node;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Note> ITEMS = new ArrayList<>();

    private static final int COUNT = 10;

    static {
        // Add some sample items.
//        for (int i = 1; i <= COUNT; i++) {
//            Note note = new Note("Note description "+i, LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli());
//            note.save();
//            ITEMS.add(note);
//        }
        ITEMS.addAll(Note.listAll(Note.class));
    }
}