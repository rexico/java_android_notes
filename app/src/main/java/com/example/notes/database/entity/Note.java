package com.example.notes.database.entity;

import android.content.Context;
import android.text.format.DateUtils;

import com.orm.SugarRecord;

public class Note extends SugarRecord {
    String desc;
    long millis;

    public Note(String desc, long millis) {
        this.desc = desc;
        this.millis = millis;
    }

    public Note() {
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getMillis() { return millis; }

    public void setMillis(long millis) {
        this.millis = millis;
    }

    public String getDateTime(Context context) {

        return DateUtils.formatDateTime(context,
                millis,
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR
                        | DateUtils.FORMAT_SHOW_TIME);
    }

}
